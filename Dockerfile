FROM openjdk:8-jdk-alpine

ADD target/jb-hello-world-maven-0.1.0.jar app.jar

EXPOSE 8080

CMD ["java" ,"-jar","./app.jar"]
